<?php
    namespace App\Libraries;

    use OAuth2\GrantType\ClientCredentials;
    use OAuth2\Server;
    use OAuth2\Storage\Pdo;

    class CiOAuth {
        public $server;
        protected $storage;
        protected $dsn;
        protected $db_username;
        protected $db_password;

        public function __construct() {
            // $this->server = getenv('database.default.hostname');
            // $this->storage = getenv('database.default.database');
            $this->dsn = 'mysql:dbname='.$this->storage.';host='.$this->server;
            $this->db_username = getenv('database.default.username');
            $this->db_password = getenv('database.default.password');
            $this->runServer();
        }

        public function runServer() {
            $this->storage = new Pdo([
                'dsn' => $this->dsn,
                'username' => $this->db_username,
                'password' => $this->db_password
            ]);
            $this->server = new Server($this->storage);
            $this->server->addGrantType(new ClientCredentials($this->storage));
        }

    }
    