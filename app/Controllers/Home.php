<?php
namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

use OAuth2\Request;


class Home extends BaseController {
    protected $model;
    public $oauth;
    protected $oauth_request;
    protected $response;

    use ResponseTrait;

    

    public function __construct () {
        $this->model = model('app\Models\MbUsers');
        $this->oauth = new \App\Libraries\CiOAuth();
        $this->oauth_request = new Request();
        

        // $this->response = 
        
    }

    public function index() {
        return view('welcome_message');   
    }

    public function login() {
        $this->response = $this->oauth->server->handleTokenRequest($this->oauth_request->createFromGlobals()); 

        // $mime = "application/{html}";
        // var_dump($this->response);exit;
        // $this->response->setContentType($mime);
        // exit;
        $code = $this->response->getStatusCode();
        $body = $this->response->getResponseBody();
        return $this->genericResponse($code, $body);
    }


    protected function genericResponse(int $code, string $body) {
        // var_dump($code);
        if ($code == 200) {
            
            return $this->respond([
                'code' => $code,
                'body' => json_decode($body),
                'authorized' => $code
            ]);
        } else {
            $data = $this->fail(json_decode($body));
            
            return $this->fail(json_decode($body));
            // return $this->fail();
        }
    }
}
